// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки – цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з’явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

let imgs = document.querySelectorAll('img');
// let start = document.body.querySelector('.start');
// let pause = document.body.querySelector('pause');
let buttons = document.body.querySelector('.buttons');


const firstImg = imgs[0];
const lastImg = imgs[imgs.length-1]


const carousel = () => {
    const activeImg = document.querySelector('.active')
    if(activeImg !== lastImg) {
        activeImg.classList.remove('active');
        activeImg.nextElementSibling.classList.add('active')
    } else {
        activeImg.classList.remove('active');
        firstImg.classList.add('active');

    }
}

let timer = setInterval(carousel, 3000);

buttons.addEventListener('click', (e) => {
    if(e.target.innerText === 'Припинити') {
  clearInterval(timer);
} else if(e.target.innerText === 'Відновити показ') {
    timer = setInterval(carousel, 3000);
}
});

// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`. 1) Таймаут спрацює один раз
// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому? 2) Планування виклику буде спрацьовувати як можна швидше, але сама функція виконається тільки, як виконається увесь код
// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен? 3) щоб запобігти виконанню функціональності, зареєстрованої setTimeout.